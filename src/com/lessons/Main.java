package com.lessons;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        String[] myWordsAndSentences =
                {"А роза упала на лапу Азора.",
                        "окО",
                        "Весна, которую все ожидали...",
                        "Я — арка края!!!",
                        "Земля",
                        "Топот"};

        for (String inputString : myWordsAndSentences) {

            if (!inputString.contains(" ")) {
                StringBuilder word = new StringBuilder(inputString).reverse();

                System.out.println("\"" + inputString + "\" - " +
                        (inputString.equalsIgnoreCase(word.toString()) ? "Палиндром." : "Не палиндром."));

            } else {
                StringBuilder sentences = new StringBuilder(
                        inputString.replace(" ", "").
                                replace(".", "").
                                replace("!", "").
                                replace("—", "").
                                replace("?", "").
                                replace(",", "").
                                replace("-", "").
                                replace(":", "").
                                replace(";", "").
                                replace("\"", "").
                                replace("(", "").
                                replace(")", ""));

                StringBuilder reverseSentences = new StringBuilder(sentences);
                reverseSentences.reverse();

                System.out.println("\"" + inputString + "\" - " +
                        (sentences.toString().equalsIgnoreCase(
                                reverseSentences.toString()) ? "Палиндром." : "Не палиндром."));
            }
            System.out.println("-----------------------------------------------");
        }


    }
}
